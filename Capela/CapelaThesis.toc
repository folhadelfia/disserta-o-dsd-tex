\select@language {portuguese}
\contentsline {chapter}{Resumo}{ix}{section*.1}
\contentsline {chapter}{\textit {Abstract}}{xi}{section*.2}
\contentsline {chapter}{Agradecimentos}{xiii}{chapter*.4}
\contentsline {chapter}{\'Indice de tabelas}{xix}{section*.7}
\contentsline {chapter}{\'Indice de figuras}{xxi}{section*.9}
\contentsline {chapter}{Gloss\'ario, acr\'onimos e abreviaturas}{xxiii}{chapter*.11}
\contentsline {chapter}{\numberline {1}Introdu\c c\~ao}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Contexto}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Motiva\c c\~oes}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Contribui\c c\~oes}{3}{section.1.3}
\contentsline {section}{\numberline {1.4}Objetivos da disserta\c c\~ao}{4}{section.1.4}
\contentsline {section}{\numberline {1.5}Organiza\c c\~ao da disserta\c c\~ao}{4}{section.1.5}
\contentsline {chapter}{\numberline {2}Estado da Arte}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Ensino \`a dist\^ancia}{8}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Enquadramento hist\'orico}{8}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Defini\c c\~oes e conceitos}{8}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Ensino eletr\'onico (e-learning)}{9}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}B-learning}{13}{subsection.2.1.4}
\contentsline {subsection}{\numberline {2.1.5}M-learning}{14}{subsection.2.1.5}
\contentsline {section}{\numberline {2.2}{\em Learning management systems}}{15}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Compara\c c\~ao de LMS}{17}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Vantagens e desvantagens}{21}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Objeto de aprendizagem}{23}{section.2.3}
\contentsline {section}{\numberline {2.4}Normaliza\c c\~ao de metadados para e-learning}{24}{section.2.4}
\contentsline {section}{\numberline {2.5}Normaliza\c c\~ao dos conte\'udos de e-learning}{25}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}AICC}{25}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}IMS}{26}{subsection.2.5.2}
\contentsline {section}{\numberline {2.6}SCORM}{27}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}{\em Content Aggregation Model} (CAM)}{28}{subsection.2.6.1}
\contentsline {subsection}{\numberline {2.6.2}{\em Run-Time Environment} (RTE)}{29}{subsection.2.6.2}
\contentsline {subsection}{\numberline {2.6.3}{\em Sequencing and Navigation} (SN)}{29}{subsection.2.6.3}
\contentsline {section}{\numberline {2.7}Tecnologias no ensino}{30}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}Realidade aumentada}{30}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}Dispositivos m\'oveis}{32}{subsection.2.7.2}
\contentsline {subsection}{\numberline {2.7.3}Plataformas multim\'edia interativas}{35}{subsection.2.7.3}
\contentsline {section}{\numberline {2.8}Arquitetura MVC ({\em Model-View-Controller})}{37}{section.2.8}
\contentsline {section}{\numberline {2.9}Frameworks}{38}{section.2.9}
\contentsline {subsection}{\numberline {2.9.1}Frameworks PHP}{39}{subsection.2.9.1}
\contentsline {subsection}{\numberline {2.9.2}Zend Framework 2}{40}{subsection.2.9.2}
\contentsline {subsection}{\numberline {2.9.3}Outras frameworks}{42}{subsection.2.9.3}
\contentsline {section}{\numberline {2.10}Conclus\~ao}{43}{section.2.10}
\contentsline {chapter}{\numberline {3}Especifica\c c\~ao da aplica\c c\~ao}{45}{chapter.3}
\contentsline {section}{\numberline {3.1}Estrutura organizacional t\IeC {\'\i }pica de um agrupamento escolar}{45}{section.3.1}
\contentsline {section}{\numberline {3.2}Especifica\c c\~ao da aplica\c c\~ao}{46}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Diagramas de Casos de Uso}{47}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Base de dados}{53}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Conceitos de interface com o utilizador --- {\em Web Design}}{55}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}Ambiente de desenvolvimento}{60}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}M\'aquina virtual}{60}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Ambiente de desenvolvimento integrado (IDE)}{61}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Zend framework 2}{61}{subsection.3.3.3}
\contentsline {chapter}{\numberline {4}Testes e Resultados}{63}{chapter.4}
\contentsline {section}{\numberline {4.1}Testes}{63}{section.4.1}
\contentsline {section}{\numberline {4.2}Resultados}{65}{section.4.2}
\contentsline {chapter}{\numberline {5}Conclus\~oes e trabalho futuro}{67}{chapter.5}
\contentsline {chapter}{Sobre o Autor}{69}{chapter*.13}
